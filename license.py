﻿def show_license(desc, year, author):
	lines = [
		desc,
		'Copyright (C) ' + year + ' ' + author,
		'This program comes with ABSOLUTELY NO WARRANTY; for details see the LICENSE.TXT.'
		'This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE.TXT for details.'
		]
	for line in lines:
		print(line)
