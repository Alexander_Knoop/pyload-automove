﻿#!/usr/bin/python
# -*- coding: utf-8 -*-
# Prüft, ob das vorgegebene Speicherlimit überschritten wurde und löst ggf. das Verschieben der Datern aus
import os, ConfigParser

# Konfiguration laden
pfad = os.path.dirname(os.path.realpath(__file__))
config = pfad + os.sep + 'config.ini'
parser = ConfigParser.SafeConfigParser()
parser.read(config)
folder = parser.get('path', 'folder')
minspace = int(parser.get('path', 'space')) # Minimaler Speicher in GB, ab dem verschoben wird

def free_space():
	# Derzeit nicht genutzt, wird aber bald integriert
	disk = os.statvfs('/')
	totalAvailSpaceNonRoot = float(disk.f_bsize*disk.f_bavail)
	return totalAvailSpaceNonRoot/1024/1024/1024

def dirsize(ordner):
	# Gibt die Größe des übergebenen Ordner in GB zurück
	folder_size = 0
	for (path, dirs, files) in os.walk(ordner):
		for file in files:
			filename = os.path.join(path, file)
			folder_size += os.path.getsize(filename)
	
	return folder_size/(1024.0*1024.0*1024.0)

s = dirsize(folder)
print 'Belegter Speicher: ' + str(s)

if s > minspace:
	# Downloads kopieren
	execfile(pfad + os.sep + 'Downloads_kopieren.py')