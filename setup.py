#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from setuptools import setup

with open('README.md') as f:
	readme = f.read()

setup(
	name='pyLoad Automove',
	version='1.1',
	description='pyload Automove - Downloads SFTP Uploader',
	url='https://bitbucket.org/Alexander_Knoop/plam',
	author='Alexander Knoop',
	author_email='computer.online@web.de',
	py_modules=['plam'],
	license='GPL-3.0',
	long_description=readme,
	)
