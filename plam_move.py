﻿#!/usr/bin/python
# -*- coding: utf-8 -*-
# Verschiebt die Dateien im Downloadverzeichnis auf den Server
import sys, os, ConfigParser, time, ntpath, pysftp
from wakeonlan import wol
from license import show_license
show_license('pyload Automove - Downloads SFTP Uploader', '2015', 'Alexander Knoop')

# Konfiguration laden
pfad = os.path.dirname(os.path.realpath(__file__))
config = pfad + os.sep + 'config.ini'
parser = ConfigParser.SafeConfigParser()
parser.read(config)
server_ip = parser.get('server', 'ip')
server_mac = parser.get('server', 'mac')
boottime = int(parser.get('server', 'boottime'))
user = parser.get('login', 'username')
password = parser.get('login', 'password')
folder = parser.get('path', 'folder')
sfolder = parser.get('server', 'folder')

# Ausschlussliste
exceptions = [
	'.chunk0', # Temporäre Datei eines laufenden Downloads
	'.chunks',
	'.crypted' # Verschlüsselte Datei von Mega.co.nz, welche von pyLoad noch entschlüsselt wird
	]

def server_starten():
	# Sendet Magic-Packet an den Server
	print 'Server wird gestartet'
	wol.send_magic_packet(server_mac)

def server_online():
	# Ping den Server an, um zu prüfen, ob dieser Online ist
	response = os.system("ping -c 1 " + server_ip)
	if response == 0:
		print 'Server online'
		return True
	else:
		print 'Server offline'
		return False

def make_list(pfad):
	# erstellt eine Liste von Dateien, die verschoben werden können
	liste = []
	for x in os.listdir(pfad):
		f = pfad + os.sep + x
		if not os.path.isfile(f):
			continue
		fileName, fileExtension = os.path.splitext(f)
		if fileExtension in exceptions:
			continue
		for ex in exceptions:
			if os.path.isfile(pfad + os.sep + fileName + ex):
				continue
		liste.append(f)
	return liste

def verschieben(liste):
	with pysftp.Connection(server_ip, username=user, password=password) as sftp:
		with sftp.cd(sfolder):
			for datei in liste:
				print 'Kopiere ' + datei
				sftp.put(datei)
				# Größenvergleich der Dateien um zu prüfen ob alles geklappt hat
				quellgroesse = os.path.getsize(datei)
				for attr in sftp.listdir_attr():
					if attr.filename == ntpath.basename(datei):
						if attr.st_size == quellgroesse:
							print 'Datei wird entfernt'
							os.remove(datei)
						else:
							print 'Es ist ein Fehler aufgetreten'


### Start

# Prüfen, ob bereits ein Kopiervorgang läuft
if os.path.isfile(pfad + os.sep + 'stop.txt'):
	print 'Stopflag ist gesetzt!'
	exit()

with open(pfad + os.sep + 'stop.txt', 'a'):
	print 'Stopflag gesetzt'

# Prüfen ob der Server online ist
if not server_online():
	# Server wecken
	server_starten()
	# Warten bis der Server hochgefahren ist
	time.sleep(boottime)
# Prüfen ob der Server jetzt verfügbar ist
if not server_online:
	print 'Der Server startet nicht'
	os.remove(pfad + os.sep + 'stop.txt')
	exit()

# Liste der zu kopierenden Daten erstellen
liste = make_list(folder)

# Dateien verschieben
verschieben(liste)

# Stopflag wieder entfernen
os.remove(pfad + os.sep + 'stop.txt')