# pyLoad Automove #

pyLoad Automove (PLAM) dient dem automatisierten Verschieben abgeschlossener Downloads von pyload oder jedem anderen Download-Programm, welches Skripte ausführen kann.

## Setup ##
Es gibt derzeit noch keine automatische Setup-Routine.

### Voraussetzungen ###
* Python 2.x
    * ConfigParser
    * pysftp
    * wakeonlan
    * ntpath

`pip install configparser pysftp wakeonlan ntpath`

### Konfiguration ###
Alle Einstellungen werden in der Datei `config.ini` vorgenommen. Da die Datei auch die Zugangsdaten zum SFTP-Server im Klartext enthält, sollten die Zugriffsrechte hier entsprechend eingeschränkt werden.

#### Server ####
Alle Einstellungen beziehen sich auf den Server, auf welchen die Daten verschoben werden sollen, nicht auf den Server, der PLAM ausführt.

##### boottime #####
Sofern der Server abgeschaltet ist, kann er von PLAM per Wake on Lan gestartet werden. Nach dem Absenden des Magic-Packets wartet PLAM die unter boottime angegebene Zeit in Sekunden. Ist der Server nach dieser Zeit noch nicht erreichbar, wird das Programm beendet.

Die Wartezeit sollte einen Puffer beinhalten, falls das System mal länger zum Starten braucht.

### plam_start.sh ###
Diese Datei dient zum automatischen Start von PLAM durch pyload. Hier müssen die Verzeichnisse entsprechend der lokalen Konfiguration angepasst werden.

Die restlichen Dateien müssen zusammen in einem anderen Verzeichnis liegen, da pyload sonst die Prüfung des belegten Speicherplatzes außer Kraft setzt und nach jedem Download die Daten verschiebt.

## Ausführung ##
PLAM kann automatisch oder manuell ausgeführt werden.

Für die manuelle Ausführung muss lediglich die Datei `plam_move.py` ausgeführt werden.

`python plam_move.py`

Beim manuellen Start werden die Daten im Downloadverzeichnis auch dann verschoben, wenn die Mindestgröße (config > space) noch nicht erreicht wurde.

Für die automatische Ausführung muss vom gewünschten Programm die Datei `plam_check.py` nach jedem abgeschlossenen Download gestartet werden. Alternativ kann das Script natürlich auch per Zeitplan z.B. mit cron gestartet werden.

# pyload Integration #

Damit PLAM von pyload gestartet wird, muss ein entsprechender Verweis gesetzt werden. Hierfür kann die Datei `plam_start.sh` in das entsprechende Verzeichnis von pyload (z.B. `~/.pyload/scripts/download_finished/`) kopiert und ausführbar gemacht werden .
Anschließend muss pyload neu gestartet werden.

## Einschränkungen ##
Das Programm ist derzeit im Beta-Status. Folgende Einschränkungen sind bekannt:

### Stopflag ###
Um zu verhindern, dass das Programm mehrfach ausgeführt wird, wird derzeit eine Datei (`stop.txt`) angelegt und nach Beendigung des Verschiebevorgangs gelöscht. Stürtzt das Programm zwischenzeitlich ab, wird die Datei nicht gelöscht und blockiert PLAM.

## Geplante Änderungen ##
* Untergrenze für Restspeicherplatz angeben